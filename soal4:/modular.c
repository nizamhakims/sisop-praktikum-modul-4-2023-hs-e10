#define FUSE_USE_VERSION 28
#include <fuse.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <fcntl.h>
#include <dirent.h>
#include <errno.h>
#include <sys/stat.h>
#include <sys/time.h>
#define PATH_MAX 4096
#define CHUNK_SIZE 1024

// Path of modular directory
const char *modularDirPath = "/home/eileithyial/prak4";
// Path path of log file
const char *logFilePath = "/home/eileithyial/fs_module.log";

// To check if directory has "module_" prefix
int isModularDir(const char *dirName)
{
    const char *prefix = "module_";
    return strncmp(dirName, prefix, strlen(prefix)) == 0;
}

// To record system call commands in a log file
void logSystemCall(const char *level, const char *command, const char *desc1, const char *desc2)
{
    time_t now = time(NULL);
    struct tm *t = localtime(&now);
    char timestamp[20];
    strftime(timestamp, sizeof(timestamp), "%y%m%d-%H:%M:%S", t);

    FILE *logFile = fopen(logFilePath, "a");
    if (logFile == NULL)
    {
        perror("Error access log file");
        return;
    }

    if (desc2 != NULL)
        fprintf(logFile, "%s::%s::%s::%s::%s\n", level, timestamp, command, desc1, desc2);
    else
        fprintf(logFile, "%s::%s::%s::%s\n", level, timestamp, command, desc1);
    fclose(logFile);
}

// To divide files into small files (chunk)
void FileChunk(const char *filePath)
{
    FILE *inputFile = fopen(filePath, "rb");
    if (inputFile == NULL)
    {
        perror("Error open file");
        return;
    }

    char chunk[CHUNK_SIZE];
    size_t bytesRead;
    int chunkIndex = 0;

    while ((bytesRead = fread(chunk, 1, sizeof(chunk), inputFile)) > 0)
    {
        char newFileName[PATH_MAX];
        snprintf(newFileName, sizeof(newFileName), "%s.%03d", filePath, chunkIndex);

        FILE *outputFile = fopen(newFileName, "wb");
        if (outputFile == NULL)
        {
            perror("Error create file");
            break;
        }
        fwrite(chunk, 1, bytesRead, outputFile);
        fclose(outputFile);
        chunkIndex++;
    }
    fclose(inputFile);
}

// merge the chunks of files
void mergeChunksToFile(const char *filePath)
{
    FILE *outputFile = fopen(filePath, "wb");
    if (outputFile == NULL)
    {
        perror("Error creating file");
        return;
    }

    int chunkIndex = 0;
    char chunkFileName[PATH_MAX];
    snprintf(chunkFileName, sizeof(chunkFileName), "%s.%03d", filePath, chunkIndex);

    while (access(chunkFileName, F_OK) == 0)
    {
        FILE *chunkFile = fopen(chunkFileName, "rb");
        if (chunkFile == NULL)
        {
            perror("Error opening chunk file");
            break;
        }
        char chunk[CHUNK_SIZE];
        size_t bytesRead;

        while ((bytesRead = fread(chunk, 1, sizeof(chunk), chunkFile)) > 0)
            fwrite(chunk, 1, bytesRead, outputFile);

        fclose(chunkFile);
        remove(chunkFileName);
        chunkIndex++;
        snprintf(chunkFileName, sizeof(chunkFileName), "%s.%03d", filePath, chunkIndex);
    }
    fclose(outputFile);
}

static int fs_getattr(const char *path, struct stat *stbuf)
{
    int res = 0;
    char fpath[PATH_MAX];

    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    res = lstat(fpath, stbuf);
    if (res == -1)
        return -errno;
    return 0;
}

static int fs_readdir(const char *path, void *buf, fuse_fill_dir_t filler,
                      off_t offset, struct fuse_file_info *fi)
{
    DIR *dp;
    struct dirent *de;
    (void)offset;
    (void)fi;
    char fpath[PATH_MAX];

    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    dp = opendir(fpath);
    if (dp == NULL)
        return -errno;

    while ((de = readdir(dp)) != NULL)
    {
        struct stat st;
        memset(&st, 0, sizeof(st));
        st.st_ino = de->d_ino;
        st.st_mode = de->d_type << 12;
        if (filler(buf, de->d_name, &st, 0))
            break;
    }
    closedir(dp);
    return 0;
}

static int fs_open(const char *path, struct fuse_file_info *fi)
{
    int res = 0;
    char fpath[PATH_MAX];
    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    res = open(fpath, fi->flags);
    if (res == -1)
        return -errno;
    close(res);
    return 0;
}

static int fs_read(const char *path, char *buf, size_t size, off_t offset, struct fuse_file_info *fi)
{
    int fd;
    int res = 0;
    (void)fi;
    char fpath[PATH_MAX];

    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    fd = open(fpath, O_RDONLY);
    if (fd == -1)
        return -errno;

    res = pread(fd, buf, size, offset);
    if (res == -1)
        res = -errno;

    close(fd);
    return res;
}

static int fs_rename(const char *from, const char *to)
{
    int res = 0;
    char ffrom[PATH_MAX];
    char fto[PATH_MAX];

    snprintf(ffrom, PATH_MAX, "%s%s", modularDirPath, from);
    snprintf(fto, PATH_MAX, "%s%s", modularDirPath, to);

    if (rename(ffrom, fto) == -1)
        res = -errno;
    if (!isModularDir(ffrom) && isModularDir(fto))
        logSystemCall("REPORT", "CREATE", fto, NULL);
    else if (isModularDir(ffrom) && !isModularDir(fto))
    {
        logSystemCall("FLAG", "RENAME", ffrom, fto);
        mergeChunksToFile(fto);
    }
    return res;
}

static int fs_rmdir(const char *path)
{
    int res = 0;
    char fpath[PATH_MAX];
    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    res = rmdir(fpath);
    if (res == -1)
        return -errno;
    logSystemCall("FLAG", "RMDIR", fpath, NULL);
    return 0;
}

static int fs_unlink(const char *path)
{
    int res = 0;
    char fpath[PATH_MAX];
    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    res = unlink(fpath);
    if (res == -1)
        return -errno;
    logSystemCall("FLAG", "UNLINK", fpath, NULL);
    return 0;
}

static int fs_mkdir(const char *path, mode_t mode)
{
    int res = 0;
    char fpath[PATH_MAX];
    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    res = mkdir(fpath, mode);
    if (res == -1)
        return -errno;
    return 0;
}

static int fs_create(const char *path, mode_t mode, struct fuse_file_info *fi)
{
    int fd;
    int res = 0;
    (void)fi;
    char fpath[PATH_MAX];
    snprintf(fpath, PATH_MAX, "%s%s", modularDirPath, path);
    fd = open(fpath, fi->flags | O_CREAT, mode);
    if (fd == -1)
        return -errno;
    close(fd);
    logSystemCall("REPORT", "CREATE", fpath, NULL);
    return res;
}

static struct fuse_operations fs_oper = {
    .getattr = fs_getattr,
    .readdir = fs_readdir,
    .open = fs_open,
    .read = fs_read,
    .rename = fs_rename,
    .rmdir = fs_rmdir,
    .unlink = fs_unlink,
    .mkdir = fs_mkdir,
    .create = fs_create,
};

int main(int argc, char *argv[])
{
    umask(0);
    return fuse_main(argc, argv, &fs_oper, NULL);
}
