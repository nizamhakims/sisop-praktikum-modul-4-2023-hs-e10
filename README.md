# Lapres Praktikum Modul 4

## Soal 1
### storage.c

#### downloadDB()
Download zip file database yang ada pada soal menggunakan command "kaggle datasets download -d bryanb/fifa-player-stats-database" melalui fork dan execvp. Tetapi, sebelum menggunakan command kaggle harus terlebih dahulu menginstall kaggle menggunakan pip pada python.
```c
void downloadDB(){
    pid_t child_id;
    child_id = fork();

    if (child_id == -1){
        exit(EXIT_FAILURE);
    }
    
    if (child_id == 0){
        char* argv[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", argv);
    }
    int status;
    wait(&status);
}
```

#### unzip()
Unzip fifa-player-stats-database.zip yang telah didownload menggunakan fork dan execvp seperti biasa
```c
void unzip(char namaZip[]){
    pid_t child_id;
    child_id = fork();

    if (child_id == -1){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *argv[] = {"unzip", namaZip, NULL};
        execvp("unzip", argv);
    }
    int status;
    wait(&status);
}
```

#### isViable
Fungsi boolean untuk menentukan apakah seorang player memenuhi persyaratan untuk direkrut. Persyaratannya yaitu age < 25, potensi > 85, dan tidak bermain untuk club "Manchester City"
```c
bool isViable(bool age, bool potensi, bool notManchester){
    if (age && potensi && notManchester){
        return true;
    }
    return false;
}
```

#### findViablePlayers()
- Buka current directory kemudian cari file dengan nama FIFA23_official_data.csv
- Setelah ditemukan, buka file FIFA23_official_data.csv dan read line by line menggunakan fgets
- Inisiasi variable boolean age, potensi, dan notManchester bernilai false
- Inisiasi variable char* token = strtok untuk mengecek string setiap tanda comma, comma disini melambangkan kolom dari database
- Inisiasi variable int col = 1 yang kemudian akan di increment setiap iterasi dari strtok
- Jika pada kolom 3, token < 25 maka age = true
- Jika pada kolom 8, token > 85 maka potensi = true
- Jika pada kolom 9, token != "Manchester City" maka notManchester = true
- Gunakan fungsi isViable untuk mengecek apakah player memenuhi persyaratan, jika memenuhi maka print data player ke terminal
```c
void findViablePlayers(){
    DIR *dir = opendir(".");
    if (dir == NULL){
        perror("Failed to open directory, exit");
        return;
    }
    
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL){
        if (strcmp(ent->d_name, "FIFA23_official_data.csv") == 0){
            char FIFA23[1000];
            sprintf(FIFA23, "%s", ent->d_name);
            
            FILE* file = fopen(FIFA23, "r");
            if (file == NULL){
                perror("Failed to open FIFA23");
                closedir(dir);
                return;
            }

            char buffer[1000];
            while (fgets(buffer, sizeof(buffer), file) != NULL){
                bool age = false, potensi = false, notManchester = false;

                char bufferCopy[1000];
                strcpy(bufferCopy, buffer);
                char* token = strtok(bufferCopy, ",");

                int col = 1;
                while (token != NULL){
                    if(col == 3){
                        int ageInt = atoi(token);
                        if(ageInt < 25)
                            age = true;
                    }
                    if (col == 8){
                        int potensiInt = atoi(token);
                        if(potensiInt > 85)
                            potensi = true;
                    }
                    if (col == 9){
                        if (strcmp(token, "Manchester City") != 0 )
                            notManchester = true;
                    }
                    col++;
                    token = strtok(NULL, ",");
                }
                bool viable = isViable(age, potensi, notManchester);
                if (viable){
                    printf("%s\n", buffer);
                }
            }
            break;
        }
    }
}
```
![](Dokumentasi/outputStorage.png)

### hubdocker.txt
Image dari storage-app dapat diakses pada link [hub.docker.com/r/nizamhakims/storage](https://hub.docker.com/r/nizamhakims/storage)

![](Dokumentasi/dockerHub.png)

### Dockerfile
Menggunakan base image ubuntu karena bisa menggunakan c dan python
```
FROM ubuntu:latest
```
Instalasi c pada container
```
RUN apt-get update && apt-get install -y build-essential
```
Instalasi python pada container
```
RUN apt-get update && apt-get install -y python3 python3-pip
```
Instalasi kaggle pada container
```
RUN apt-get update && pip install kaggle
```
Instalasi unzip pada container
```
RUN apt-get update && apt-get install -y unzip
```
Copy API kaggle ke container
```
COPY kaggle.json /root/.kaggle/kaggle.json
```
Change permission kaggle.json agar hanya bisa di read dan write oleh owner
```
RUN chmod 600 /root/.kaggle/kaggle.json
```
Buat directory main dan set directory main menjadi working directory
```
RUN mkdir /main
WORKDIR /main
```
Copy storage.c ke directory main di dalam container
```
COPY storage.c .
```
Compile storage.c menggunakan gcc
```
RUN gcc -o storage storage.c
```
Run ./storage
```
CMD [ "./storage" ]
```
![](Dokumentasi/dockerFileBuild.png)

### docker-compose.yml
- Menggunakan docker-compose version 3
- Membuat 5 services dengan image sama yaitu nizamhakims/storage:latest tetapi dengan 5 ports berbeda yaitu 3000--3004
```
version: '3'
services:
  storage1:
    image: nizamhakims/storage:latest
    ports:
      - "3000:3000"
  storage2:
    image: nizamhakims/storage:latest
    ports:
      - "3001:3001"
  storage3:
    image: nizamhakims/storage:latest
    ports:
      - "3002:3002"
  storage4:
    image: nizamhakims/storage:latest
    ports:
      - "3003:3003"
  storage5:
    image: nizamhakims/storage:latest
    ports:
      - "3004:3004"
```
![](Dokumentasi/dockerComposeBarcelona.png)

## Soal 4
### Penjelasan Algoritma
1. Deklarasi
- Baris #define FUSE_USE_VERSION 28 menentukan versi FUSE yang digunakan
- Header-file yang dibutuhkan untuk mengimplementasikan sistem file FUSE
- Definisi konstanta menggunakan #define, seperti PATH_MAX dengan nilai 4096 dan CHUNK_SIZE dengan nilai 1024. Konstanta ini digunakan untuk menentukan ukuran buffer atau path maksimum dalam program
- Deklarasi variabel modularDirPath dan logFilePath yang menyimpan path direktori modular dan path file log

2. Fungsi isModularDir()
Digunakan untuk memeriksa apakah sebuah direktori memiliki awalan "module_". Fungsi ini mengembalikan nilai 1 jika direktori memiliki awalan tersebut, dan 0 jika tidak.

3. Fungsi logSystemCall()
- Fungsi ini menerima empat argumen: level, command, desc1, dan desc2
    - Argumen level mengindikasikan tingkat (level) log, seperti "REPORT" atau "FLAG"
    - Argumen command merupakan perintah sistem yang dicatat
    - Argumen desc1 dan desc2 adalah deskripsi terkait perintah tersebut
- Fungsi ini menggunakan fungsi time() untuk mendapatkan waktu saat ini dalam format UNIX timestamp
- Struct tm dan variabel t digunakan untuk mengonversi UNIX timestamp menjadi struktur yang berisi informasi waktu seperti tahun, bulan, hari, jam, menit, dan detik. Fungsi localtime() digunakan untuk melakukan konversi tersebut
- Variabel timestamp digunakan untuk menyimpan string waktu yang telah diformat sesuai dengan format yang diinginkan ("%y%m%d-%H:%M:%S")
- Fungsi membuka file log menggunakan fungsi fopen() dengan mode "a" (append). Jika file tidak dapat diakses atau dibuka, fungsi perror() akan mencetak pesan kesalahan yang sesuai
- Fungsi ini mencetak perintah sistem ke dalam file log menggunakan fungsi fprintf()

--> Jika desc2 tidak NULL, format yang digunakan adalah "%s::%s::%s::%s::%s", di mana level, timestamp, command, desc1, dan desc2 dimasukkan ke dalam string yang dicetak.
--> Jika desc2 NULL, format yang digunakan adalah "%s::%s::%s::%s", di mana level, timestamp, command, dan desc1 dicetak.g. File log ditutup menggunakan fungsi fclose() setelah mencetak perintah sistem.

4. Fungsi FileChunk()
- Fungsi ini menerima satu argumen yaitu filePath, yang merupakan jalur file yang akan dibagi menjadi chunk
- Fungsi membuka file input menggunakan fungsi fopen() dengan mode "rb" (read binary). Jika file tidak dapat dibuka, fungsi perror() akan mencetak pesan kesalahan yang sesuai, dan fungsi akan mengembalikan
- Sebuah buffer chunk dengan ukuran CHUNK_SIZE (1024 dalam kode ini) digunakan untuk membaca data dari file input
- Variabel bytesRead bertipe size_t digunakan untuk menyimpan jumlah byte yang dibaca dari file input setiap kali fread() dipanggil
- Variabel chunkIndex digunakan untuk mengindeks chunk yang dibuat. Nilainya dimulai dari 0
- Dalam loop while, fungsi membaca CHUNK_SIZE byte data dari file input ke dalam buffer chunk menggunakan fungsi fread(). Jika jumlah byte yang dibaca lebih dari 0, artinya masih ada data yang harus dibaca
- Fungsi membuat nama file baru untuk setiap chunk yang dibuat dengan menggunakan fungsi snprintf(). Nama file baru terdiri dari filePath yang ditambahkan dengan indeks chunk pada bagian akhir (dalam format ".xxx", misalnya ".001", ".002", dst.)
- Fungsi membuka file output baru dengan nama file baru menggunakan fungsi fopen() dengan mode "wb" (write binary). Jika file tidak dapat dibuat, fungsi perror() akan mencetak pesan kesalahan yang sesuai, dan loop while dihentikan dengan menggunakan break
- Data yang ada di dalam buffer chunk ditulis ke file output menggunakan fungsi fwrite()
- Setelah data ditulis, file output ditutup menggunakan fungsi fclose()
- Variabel chunkIndex ditingkatkan nilainya untuk menandakan pembuatan chunk baru
- Proses di atas akan diulangi hingga semua data dalam file input dibaca dan dibagi menjadi chunk
- File input ditutup menggunakan fungsi fclose() setelah selesai

5. Fungsi mergeChunksToFile()
- Fungsi ini menerima satu argumen yaitu filePath, yang merupakan jalur file utuh yang akan dihasilkan setelah menggabungkan chunk
- Fungsi membuka file output menggunakan fungsi fopen() dengan mode "wb" (write binary). Jika file tidak dapat dibuat, fungsi perror() akan mencetak pesan kesalahan yang sesuai, dan fungsi akan mengembalikan
- Variabel chunkIndex digunakan untuk mengindeks chunk yang akan digabungkan. Nilainya dimulai dari 0
- Sebuah buffer chunkFileName dengan ukuran PATH_MAX digunakan untuk menyimpan nama file chunk
- Menggunakan fungsi snprintf(), fungsi membuat nama file chunk awal dengan menggabungkan filePath dengan indeks chunk pada bagian akhir (dalam format ".xxx", misalnya ".001", ".002", dst.)
- Dalam loop while, fungsi memeriksa apakah file chunk dengan nama yang dihasilkan oleh chunkFileName ada atau tidak menggunakan fungsi access() dengan mode F_OK. Jika file tersebut ada, loop akan dijalankan
- Di dalam loop, fungsi membuka file chunk dengan nama chunkFileName menggunakan fungsi fopen() dengan mode "rb" (read binary). Jika file tidak dapat dibuka, fungsi perror() akan mencetak pesan kesalahan yang sesuai, dan loop while dihentikan dengan menggunakan break
- Selanjutnya, fungsi membaca data dari file chunk menggunakan fungsi fread() dan menuliskannya ke file output menggunakan fungsi fwrite(). Proses ini diulangi sampai semua data dalam file chunk dibaca
- Setelah selesai membaca data dari file chunk, file chunk tersebut ditutup menggunakan fungsi fclose()
- Fungsi menghapus file chunk menggunakan fungsi remove() agar tidak diperlukan lagi setelah data diambil. Hal ini memastikan bahwa setiap chunk hanya digabungkan sekali ke dalam file utuh
- Variabel chunkIndex ditingkatkan nilainya untuk mengindeks chunk berikutnya
- Menggunakan fungsi snprintf(), fungsi membuat nama file chunk berikutnya berdasarkan filePath dan chunkIndex yang baru
- Proses di atas diulangi hingga tidak ada lagi file chunk yang ditemukan
- File output ditutup menggunakan fungsi fclose() setelah selesai menggabungkan semua chunk

6. Fungsi Operasi Fuse 
- fs_getattr(): Mengambil atribut file (metadata) seperti mode, ukuran, dan waktu modifikasi
- fs_readdir(): Membaca direktori dan mengisi entri direktori ke dalam buffer.• fs_open(): Membuka file
- fs_read(): Membaca data dari file
- fs_rename(): Mengganti nama file atau memindahkan file
- fs_rmdir(): Menghapus direktori kosong
- fs_unlink(): Menghapus file
- fs_mkdir(): Membuat direktori
- fs_create(): Membuat file baru

### Compile Module.c 
1. Perintah Compile
`gcc -o myfs modular.c -lfuse -D_FILE_OFFSET_BITS=64`

2. Membuat direktori mount
`mkdir my_mount`

3. Mount fuse pada direktori mount
`./myfs my_mount`

4. Pindah ke direktori mount 
`cd my_mount`

5. Buat module directory
`mkdir module_directory`

6. Pindah ke direktori module
`cd module_directory`

7. Membuat file txt
`touch module_file.txt`
`touch module_file_new.txt`

8. Menghapus file
`rm module_file.txt`

9. Menghapus direktori module
`rmdir module_directory`

10. Melihat log kegiatan
`cat my_mount/fs_module.log`

### Dokumentasi
![](Dokumentasi/modular.jpg)
![](Dokumentasi/fs_module.jpg)

## Soal 5
