#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/wait.h>
#include <dirent.h>
#include <stdbool.h>

void downloadDB(){
    pid_t child_id;
    child_id = fork();

    if (child_id == -1){
        exit(EXIT_FAILURE);
    }
    
    if (child_id == 0){
        char* argv[] = {"kaggle", "datasets", "download", "-d", "bryanb/fifa-player-stats-database", NULL};
        execvp("kaggle", argv);
    }
    int status;
    wait(&status);
}

void unzip(char namaZip[]){
    pid_t child_id;
    child_id = fork();

    if (child_id == -1){
        exit(EXIT_FAILURE);
    }

    if (child_id == 0){
        char *argv[] = {"unzip", namaZip, NULL};
        execvp("unzip", argv);
    }
    int status;
    wait(&status);
}

bool isViable(bool age, bool potensi, bool notManchester){
    if (age && potensi && notManchester){
        return true;
    }
    return false;
}

void findViablePlayers(){
    DIR *dir = opendir(".");
    if (dir == NULL){
        perror("Failed to open directory, exit");
        return;
    }
    
    struct dirent *ent;
    while ((ent = readdir(dir)) != NULL){
        if (strcmp(ent->d_name, "FIFA23_official_data.csv") == 0){
            char FIFA23[1000];
            sprintf(FIFA23, "%s", ent->d_name);
            
            FILE* file = fopen(FIFA23, "r");
            if (file == NULL){
                perror("Failed to open FIFA23");
                closedir(dir);
                return;
            }

            char buffer[1000];
            while (fgets(buffer, sizeof(buffer), file) != NULL){
                bool age = false, potensi = false, notManchester = false;

                char bufferCopy[1000];
                strcpy(bufferCopy, buffer);
                char* token = strtok(bufferCopy, ",");

                int col = 1;
                while (token != NULL){
                    if(col == 3){
                        int ageInt = atoi(token);
                        if(ageInt < 25)
                            age = true;
                    }
                    if (col == 8){
                        int potensiInt = atoi(token);
                        if(potensiInt > 85)
                            potensi = true;
                    }
                    if (col == 9){
                        if (strcmp(token, "Manchester City") != 0 )
                            notManchester = true;
                    }
                    col++;
                    token = strtok(NULL, ",");
                }
                bool viable = isViable(age, potensi, notManchester);
                if (viable){
                    printf("%s\n", buffer);
                }
            }
            break;
        }
    }
}
int main(){
    downloadDB();
    unzip("fifa-player-stats-database.zip");
    findViablePlayers();
}